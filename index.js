var express = require("express"),
    handlebars = require('express-handlebars');

var bodyParser = require('body-parser');
	
var app = express();

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

var posts = [{
				"subject": "Post numero 1", 
				"description": "Descripcion 1", 
				"time": new Date()
			},
			{
				"subject": "Post numero 2", 
				"description": "Descripcion 2", 
				"time": new Date()
			}];

app.engine('handlebars', handlebars());
app.set('view engine', 'handlebars');
app.set("views", "./views");


app.get('/posts', function(req, res){
    res.render('posts', { "title": "Hola gente", "posts" : posts} );
});

app.post('/posts', function(req, res){
	
	posts.push(req.body);
	
    res.end(JSON.stringify(req.body));
	
	//res.end("OK");
});

app.listen(8080);